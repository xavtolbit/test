package iam.toledano.xavi.uf3.exc1;

public class Multiplica {
	
	private int num1;
	private int num2;
	
	public Multiplica(int num1, int num2) {
		this.num1 = num1;
		this.num2 = num2;
	}
	
	public int multiplica() {
		return num1 * num2;
	}
}
