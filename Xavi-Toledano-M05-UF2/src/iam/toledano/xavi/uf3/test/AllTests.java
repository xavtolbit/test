package iam.toledano.xavi.uf3.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestMultiplica.class, TestSuma.class })
public class AllTests {

}
