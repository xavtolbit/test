package iam.toledano.xavi.uf3.test;

import static org.junit.Assert.*;

import org.junit.Test;

import iam.toledano.xavi.uf3.exc1.Multiplica;

public class TestMultiplica {

	@Test
	public void testMultiplica() {
		Multiplica multiplica = new Multiplica(2,3);
		assertEquals(6, multiplica.multiplica());
	}

}
