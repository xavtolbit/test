package iam.toledano.xavi.uf3.test;

import static org.junit.Assert.*;

import org.junit.Test;

import iam.toledano.xavi.uf3.exc1.Suma;

public class TestSuma {

	@Test
	public void testSuma() {
		Suma suma = new Suma(2,3);
		assertEquals(5, suma.suma());
	}

}
